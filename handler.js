const Parser = require('rss-parser');
const AWS = require('aws-sdk');
const axios = require('axios').default;
const _ = require('lodash');
const TableName = process.env.DYNAMODB_TABLE;

const getTodaysKey = () => (new Date()).getUTCDate() + '/' + ((new Date()).getUTCMonth() + 1) + '/' + (new Date()).getUTCFullYear();

const getRSSPosts = async url => {
  console.log(`getting posts for ${url}`);
  const parser = new Parser({
    headers: { 'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0' },
  });
  const feed = await parser.parseURL(url);

  let items = {};
  for (let i = 0; i < feed.items.length; i++) {
    let item = feed.items[i];
    const regex = /<a.*?href=\"([^\"\']*?)\">\[link\]<\/a>/ig;
    m = regex.exec(item.content);

    switch (true) {
      case m === null:
      case m.length < 2:
      case m[1].indexOf('redd') >= 0:
      case m[1].indexOf('soundcloud') >= 0:
      case m[1].indexOf('bandcamp') >= 0:
      case item.title.toLowerCase().indexOf('how do i '):
        continue;
    }

    items[m[1]] = {
      title: item.title,
      published: Date.parse(item.pubDate)
    };
  }

  return items;
};

const getWordpressStories = async host => {
  const regex = /(?:href=\\*?")(https?:\/\/.+?\.[a-zA-Z\.]+\/?(?:[a-zA-Z0-9\/\-\_])*)\\*"/gi;
  const res = await axios.get(`${host}/wp-json/wp/v2/posts?per_page=30`);
  const posts = {};

  for (let i = 0; i < res.data.length; i++) {
    let post = res.data[i];
    let title = post.title.rendered;
    let content = post.content.rendered;
    let published = Date.parse(post.date);
    let matches = regex.exec(content);

    if (matches === null) {
      continue;
    }

    let url = false;
    for (let j = 0; j < matches.length; j++) {
      let match = matches[j];
      switch (true) {
        case match.indexOf('wp.com') >= 0:
        case match.indexOf('synthtopia') >= 0:
        case match.indexOf('xlr8r') >= 0:
        case match.indexOf('djcity') >= 0:
        case match.indexOf('instagram') >= 0:
        case match.match(/\.(jpeg|jpg|gif|png)$/) != null:
        case match.match(/\.(mp3|mp4|ogg|flac|torrent|aac|opus)$/) != null:
          continue;
      }
      url = match;
    }

    if (!url) {
      continue;
    }

    posts[url] = {
      title,
      published
    };
  };

  return posts;
};


const getHNStories = async keyword => {
  const parser = new Parser();
  const feed = await parser.parseURL(`https://hnrss.org/newest?q=${keyword}`);

  let items = {};
  for (let i = 0; i < feed.items.length; i++) {
    let item = feed.items[i];
    if (item.link.indexOf('ycombinator') >= 0) {
      continue;
    }

    items[item.link] = {
      title: item.title.replace('Show HN: ', ''),
      published: Date.parse(item.pubDate)
    };
  }

  return items;
};

const getCurrentItems = async () => {
  const dynamoDB = new AWS.DynamoDB({ apiVersion: '2012-08-10' });

  let items = {};
  const data = await dynamoDB.getItem({
    TableName,
    Key: {
      "date": {
        S: getTodaysKey()
      }
    },
  }).promise();

  if (typeof data.Item !== 'undefined') {
    items = JSON.parse(data.Item.data.S);
  }

  return items;
};

const setNewItems = async items => {
  const dynamoDB = new AWS.DynamoDB({ apiVersion: '2012-08-10' });
  const params = {
    TableName,
    Item: {}
  };
  params.Item.date = {};
  params.Item.data = {};
  params.Item.date.S = getTodaysKey();
  params.Item.data.S = JSON.stringify(items);
  const res = await dynamoDB.putItem(params).promise();

  return res;
};

const mergeItems = async newItems => {
  if (typeof newItems === 'undefined') {
    throw new Error('new items are undefined');
  }
  let items = await getCurrentItems();
  _.merge(items, newItems);
  await setNewItems(items);
  console.log(`new item count: ${Object.keys(items).length}`);
};

const searchHN = async keyword => {
  const hnItems = await getHNStories(keyword);
  await mergeItems(hnItems);
};

const searchResidentAdvisor = async keyword => {
  const newItems = await getRSSPosts(`https://www.residentadvisor.net/xml/${keyword}.xml`);
  await mergeItems(newItems);
};

module.exports.getLandr = async e => {
  const newItems = await getRSSPosts('https://blog.landr.com/feed/');
  await mergeItems(newItems);
  return 'success';
};

module.exports.getHN = async e => {
  await searchHN('dj');
  await searchHN('music production');
  return 'success';
};

module.exports.getResidentAdvisor = async e => {
  await searchResidentAdvisor('rss_news');
  await searchResidentAdvisor('the-feed');
  return 'success';
};

module.exports.getSynthopia = async e => {
  let newItems = await getWordpressStories('http://www.synthtopia.com');
  await mergeItems(newItems);
  return 'success';
};

module.exports.getDJCity = async e => {
  let newItems = await getWordpressStories('https://news.djcity.com');
  await mergeItems(newItems);
  return 'success';
};

module.exports.getXlr8r = async e => {
  let newItems = await getWordpressStories('https://www.xlr8r.com');
  await mergeItems(newItems);
  return 'success';
};